# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Feature Iterator
qgisMinimumVersion=2.0
description=Iterate Layer by keyboard, selecting each feature (wiht optional screenshot)
version=0.1
author=David Vaz
email=david@viadipuk.pt

about=Iterate a Layer with keyboard shortcuts. Each feature is selected and auto zoomed by pressing right key. Press up to zoom out to the hole layer. Press p to take a screenshot of current window.

tracker=https://gitlab.com/davidmgvaz/qgis_iterate_features/issues
repository=https://gitlab.com/davidmgvaz/qgis_iterate_features
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=

homepage=
category=Plugins
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

