# -*- coding: utf-8 -*-
"""
/***************************************************************************
 IterateFeatures
                                 A QGIS plugin
 Iterate Layer by keyboard, selecting each feature (wiht optional screenshot)
                             -------------------
        begin                : 2016-10-25
        copyright            : (C) 2016 by David Vaz
        email                : david@viadipuk.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load IterateFeatures class from file IterateFeatures.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .iterate_features import IterateFeatures
    return IterateFeatures(iface)
