# -*- coding: utf-8 -*-
# ***************************************************************************
#
# Iterate through a vector layer features by pressing the SpaceBar
#
# Copyright (C) 2015 Germán Carrillo  (geotux_tuxman@linuxmail.org)
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify        *
# *   it under the terms of the GNU General Public License as published by        *
# *   the Free Software Foundation; either version 2 of the License, or           *
# *   (at your option) any later version.                                      *
# *                                                                         *
# ***************************************************************************
from qgis.utils import iface
from PyQt4.QtGui import QShortcut, QKeySequence
from PyQt4.QtCore import Qt

myIt = None
activeLayerId = ""
selectedFeature = None
kind = 'detail'

def save_screenshoot(id):
    global kind
    
    if id is None:
        return
    path = '/Users/david/Downloads/teste/%s_%d.png' % (kind, id)
    print 'saving:', path
    iface.mapCanvas().saveAsImage(path)

def escapePressed():
    global myIt, selectedFeature
    myIt = None
    selectedFeature = None
    print "Iteration finished!"

def spaceBarPressed():
    global myIt, activeLayerId, selectedFeature, kind
    aLayer = iface.activeLayer()
    if not aLayer or not aLayer.type() == 0:
        print "Please first select a vector layer in the ToC."
        return
    if activeLayerId != aLayer.id():
        activeLayerId = aLayer.id()
        myIt = None
    if not myIt:
        myIt = aLayer.getFeatures()

    feat = next( myIt, None )
    if feat:
        aLayer.removeSelection()
        save_screenshoot(selectedFeature)
        selectedFeature = feat.id()
        aLayer.select( selectedFeature )
        iface.actionZoomToSelected().trigger()
        kind = 'detail'
        print "Selected feature:",str( selectedFeature )
    else:
        print "We reached the last feature of this layer already.\n" + \
            "If you want to restart press the Escape key."


def altPressed():
    global kind
    aLayer = iface.activeLayer()
    if not aLayer or not aLayer.type() == 0:
        print "Please first select a vector layer in the ToC."
        return
    print "Overview Zoom"
    iface.actionZoomToLayer().trigger()
    kind = 'overview'

    
shortcutEnter = QShortcut(QKeySequence(Qt.Key_Escape), iface.mapCanvas())
shortcutEnter.setContext(Qt.ApplicationShortcut)
shortcutEnter.activated.connect(escapePressed)

shortcutAlt = QShortcut(QKeySequence(Qt.Key_V), iface.mapCanvas())
shortcutAlt.setContext(Qt.ApplicationShortcut)
shortcutAlt.activated.connect(altPressed)

shortcutSpaceBar = QShortcut(QKeySequence(Qt.Key_Space), iface.mapCanvas())
shortcutSpaceBar.setContext(Qt.ApplicationShortcut)
shortcutSpaceBar.activated.connect(spaceBarPressed)
