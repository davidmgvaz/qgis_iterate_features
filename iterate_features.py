# -*- coding: utf-8 -*-
"""
/***************************************************************************
 IterateFeatures
                                 A QGIS plugin
 Iterate Layer by keyboard, selecting each feature (wiht optional screenshot)
                              -------------------
        begin                : 2016-10-25
        git sha              : $Format:%H$
        copyright            : (C) 2016 by David Vaz
        email                : david@viadipuk.pt
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QDir
from PyQt4.QtCore import Qt, QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon, QKeySequence, QFileDialog
from qgis.core import QgsFeatureRequest, QgsMessageLog

# Initialize Qt resources from file resources.py
import resources

# Import the code for the dialog
from iterate_features_dialog import IterateFeaturesDialog
import os.path


# noinspection PyCallByClass,PyPep8Naming
class IterateFeatures:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'IterateFeatures_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                # noinspection PyArgumentList
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = IterateFeaturesDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Feature Iterator')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'IterateFeatures')
        self.toolbar.setObjectName(u'IterateFeatures')

        # state vars
        self.layer = None
        self.layers = []
        self.featuresIds = []
        self.selected_feature_index = -1
        self.fields = []
        self.selected_field_index = 0
        self.zoomLevelPrefix = self.dlg.overviewPrefix.text()

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('IterateFeatures', message)

    def add_action(
            self,
            icon_path,
            text,
            callback,
            enabled_flag=True,
            add_to_menu=True,
            add_to_toolbar=True,
            status_tip=None,
            whats_this=None,
            parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    # noinspection PyAttributeOutsideInit,PyPep8Naming
    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/IterateFeatures/icon.png'
        self.actionConfigure = self.add_action(
            icon_path,
            text=self.tr(u'Iterate Features'),
            callback=self.configure,
            parent=self.iface.mainWindow())
        self.actionConfigure.setShortcut(QKeySequence(Qt.Key_F6))

        icon_path = ':/plugins/IterateFeatures/icon.png'
        self.actionNext = self.add_action(
            icon_path,
            text=self.tr(u'Next Feature'),
            callback=self.nextFeature,
            parent=self.iface.mainWindow())
        self.actionNext.setShortcut(QKeySequence(Qt.Key_Space))
        self.actionNext.setDisabled(True)

        icon_path = ':/plugins/IterateFeatures/icon.png'
        self.actionPrev = self.add_action(
            icon_path,
            text=self.tr(u'Previous Feature'),
            callback=self.previousFeature,
            parent=self.iface.mainWindow())
        self.actionPrev.setShortcut(QKeySequence(Qt.Key_B))
        self.actionPrev.setDisabled(True)

        icon_path = ':/plugins/IterateFeatures/icon.png'
        self.actionZoomToLayer = self.add_action(
            icon_path,
            text=self.tr(u'Zoom To Layer'),
            callback=self.zoomToLayer,
            parent=self.iface.mainWindow())
        self.actionZoomToLayer.setShortcut(QKeySequence(Qt.Key_V))
        self.actionZoomToLayer.setDisabled(True)

        icon_path = ':/plugins/IterateFeatures/icon.png'
        self.actionZoomToSelected = self.add_action(
            icon_path,
            text=self.tr(u'Zoom to Feature'),
            callback=self.zoomToSelected,
            parent=self.iface.mainWindow())
        self.actionZoomToSelected.setShortcut(QKeySequence(Qt.Key_C))
        self.actionZoomToSelected.setDisabled(True)

        icon_path = ':/plugins/IterateFeatures/icon.png'
        self.actionPrintScreen = self.add_action(
            icon_path,
            text=self.tr(u'Print Screen'),
            callback=self.print_screen,
            parent=self.iface.mainWindow())
        self.actionPrintScreen.setShortcut(QKeySequence(Qt.Key_N))
        self.actionPrintScreen.setDisabled(True)

        self.dlg.layerComboBox.currentIndexChanged.connect(self.set_layer_attributes)

        self.dlg.printScreenOption.toggled.connect(self.set_print_screen_option)
        self.dlg.outputDirectoryButton.clicked.connect(self.select_output_directory)
        self.dlg.attributeComboBox.currentIndexChanged.connect(self.set_attribute_selected)

        settings = QSettings()
        output_directory = settings.value('/IterateFeatures/outputDirectory', '', type=str)
        if QDir(output_directory).exists():
            self.dlg.outputDirectory.setText(output_directory)
        self.dlg.detailPrefix.setText(settings.value('/IterateFeatures/detailPrefix', 'detail', type=str))
        self.dlg.overviewPrefix.setText(settings.value('/IterateFeatures/overviewPrefix', 'overview', type=str))

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Feature Iterator'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def configure(self):
        """Run method that performs all the real work"""

        QgsMessageLog.logMessage('Configure', 'IterateFeatures')

        self.layers = self.iface.legendInterface().layers()

        layer_list = []
        index = 0
        selected_layer_index = 0
        for layer in self.layers:
            QgsMessageLog.logMessage(layer.name(), 'IterateFeatures')
            QgsMessageLog.logMessage(unicode(type(layer)), 'IterateFeatures')
            QgsMessageLog.logMessage(unicode(layer.type()), 'IterateFeatures')
            if layer.type() == 0:
                layer_list.append(layer.name())
                if layer == self.layer:
                    selected_layer_index = index
                index += 1

        self.dlg.layerComboBox.clear()
        self.dlg.layerComboBox.addItems(layer_list)
        self.dlg.layerComboBox.setCurrentIndex(selected_layer_index)

        QgsMessageLog.logMessage('Selected Layer Index' + str(selected_layer_index), 'IterateFeatures')

        self.dlg.show()
        result = self.dlg.exec_()
        if result:
            selected_layer_index = self.dlg.layerComboBox.currentIndex()
            self.layer = self.layers[selected_layer_index]
            self.featuresIds = self.layer.allFeatureIds()
            self.selected_feature_index = -1

            self.updateActionConfigureText()
            self.actionNext.setDisabled(False)
            self.actionPrev.setDisabled(False)
            self.actionZoomToLayer.setDisabled(False)
            self.actionZoomToSelected.setDisabled(False)

            self.iface.setActiveLayer(self.layer)

            if self.dlg.printScreenOption.isChecked():
                settings = QSettings()
                settings.setValue('/IterateFeatures/outputDirectory', self.dlg.outputDirectory.text())
                settings.setValue('/IterateFeatures/detailPrefix', self.dlg.detailPrefix.text())
                settings.setValue('/IterateFeatures/overviewPrefix', self.dlg.overviewPrefix.text())

            QgsMessageLog.logMessage('Result', 'IterateFeatures')

    def select_output_directory(self):
        directory = QFileDialog.getExistingDirectory(self.dlg, "Select Output directory", "")
        self.dlg.outputDirectory.setText(directory)

    def set_layer_attributes(self, selected_layer_index):
        QgsMessageLog.logMessage(
            'Set_layer_attributes:' + str(selected_layer_index) + ',' + str(self.selected_feature_index),
            'IterateFeatures')
        if self.dlg.printScreenOption.isChecked() and self.selected_feature_index != selected_layer_index:
            QgsMessageLog.logMessage('Set_layer_attributes RESET:' + str(selected_layer_index), 'IterateFeatures')
            layer = self.layers[selected_layer_index]
            self.fields = layer.fields()
            fieldnames = [field.name() for field in self.fields]
            QgsMessageLog.logMessage('Fields: ' + str(fieldnames), 'IterateFeatures')
            self.dlg.attributeComboBox.clear()
            self.dlg.attributeComboBox.addItems(fieldnames)
            self.selected_field_index = 0

    def set_attribute_selected(self, selected_field_index):
        self.selected_field_index = selected_field_index

    def set_print_screen_option(self, active):
        if active:
            self.set_layer_attributes(self.dlg.layerComboBox.currentIndex())
            self.actionPrintScreen.setDisabled(False)
        else:
            self.actionPrintScreen.setDisabled(True)

    def updateActionConfigureText(self):
        if self.layer is not None:
            self.actionConfigure.setText(self.tr(u'Iterate Features') + ': ' + self.layer.name())
        else:
            self.actionConfigure.setText(self.tr(u'Iterate Features'))

    def nextFeature(self):
        QgsMessageLog.logMessage('Next Pressed', 'IterateFeatures')

        self.selected_feature_index += 1
        if self.selected_feature_index > len(self.featuresIds) - 1:
            self.selected_feature_index = -1
            self.layer.removeSelection()
            self.zoomToLayer()
            return

        self.selectFeature()
        self.zoomToSelected()

    def previousFeature(self):
        QgsMessageLog.logMessage('Prev Pressed', 'IterateFeatures')

        self.selected_feature_index -= 1
        if self.selected_feature_index < 0:
            self.selected_feature_index = -1
            self.layer.removeSelection()
            self.zoomToLayer()
            return

        self.selectFeature()
        self.zoomToSelected()

    def selectFeature(self):
        self.layer.removeSelection()
        self.layer.select(self.featuresIds[self.selected_feature_index])

    def zoomToSelected(self):
        self.iface.actionZoomToSelected().trigger()
        self.zoomLevelPrefix = self.dlg.detailPrefix.text()

    def zoomToLayer(self):
        self.iface.actionZoomToLayer().trigger()
        self.zoomLevelPrefix = self.dlg.overviewPrefix.text()

    def print_screen(self):
        feature = next(
            self.layer.getFeatures(QgsFeatureRequest().setFilterFid(self.featuresIds[self.selected_feature_index])))
        QgsMessageLog.logMessage(str(feature) + ':' + str(feature.id()), 'IterateFeatures')

        path = QDir(self.dlg.outputDirectory.text()).absoluteFilePath(
            u"%s%s.png" % (self.zoomLevelPrefix,
                           unicode(feature.attribute(self.dlg.attributeComboBox.currentText()))
        ))

        QgsMessageLog.logMessage('saving: ' + path, 'IterateFeatures')

        self.iface.mapCanvas().saveAsImage(path)
